﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

static class MainStart
{
	/// <summary>
	/// 应用程序的主入口点。
	/// </summary>
	[STAThread]
	static void Main()
    {
        bool noRun = false;
        System.Threading.Mutex Run = new System.Threading.Mutex(true, "GameTools", out noRun);
        //检测是否已经运行
        if (noRun)
        {
            Run.ReleaseMutex();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
        else
        {
            Application.Exit();
        }
	} 
}