﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

class FileManager
{
	//读取一个文件，返回字符串
	public static string readFile(string path)
	{
		StringBuilder sb = new StringBuilder();

		StreamReader objReader = new StreamReader(path);

		string sLine = "";
		while (sLine != null)
		{
			sLine = objReader.ReadLine();
			if (sLine != null && !sLine.Equals(""))
			{
				sb.Append(sLine);
				sb.AppendLine();
			}
		}

		objReader.Close();

		return sb.ToString();
	}

	//创建文件夹
	public static bool createDirectory(string dirPath)
	{
		if (!Directory.Exists(dirPath))
		{
			Directory.CreateDirectory(dirPath);
			return true;
		}
		return false;
	}
	//创建文件
	public static bool createFile(string path)
	{
		if (!File.Exists(path))
		{
			FileStream f = File.Create(path);
			f.Close();
			return true;
		}
		return false;
	}

	//写入文件
	public static void writeFile(string path, string content)
	{
		FileStream fs = new FileStream(path, FileMode.Create);
		//获得字节数组
		byte[] data = new UTF8Encoding().GetBytes(content);
		//开始写入
		fs.Write(data, 0, data.Length);
		//清空缓冲区、关闭流
		fs.Flush();
		fs.Close();
	}

	//读取某文件夹下的所有文件，返回List<文件路径>
	public static List<string> getDirectoryAllFilePath(string dirPath)
	{
		List<string> list = new List<string>();

		DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
		if (dirInfo.Exists)
		{
			FileInfo[] fileInfoArray = dirInfo.GetFiles();
			foreach (FileInfo f in fileInfoArray)
			{
				list.Add(f.FullName);
			}
		}
		else
		{
			return null;
		}

		return list;
	}
	//读取某文件夹下的所有文件，返回List<文件路径>
	public static FileInfo[] getDirectoryAllFile(string dirPath)
	{
		FileInfo[] list;

		DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
		if (dirInfo.Exists)
		{
			list = dirInfo.GetFiles();
		}
		else
		{
			return null;
		}

		return list;
	}
	//读取某文件夹下的所有文件，返回List<内容>
	public static List<string> readDirectoryAllFile(string dirPath)
	{
		List<string> list = new List<string>();

		DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
		if (dirInfo.Exists)
		{
			FileInfo[] fileInfoArray = dirInfo.GetFiles();
			foreach (FileInfo f in fileInfoArray)
			{
				string s = readFile(f.FullName);
				list.Add(s);
			}
		}
		else
		{
			return null;
		}

		return list;
	}
}
