﻿using System.Runtime.Serialization;

[DataContract(Namespace = "http://coderzh.cnblogs.com")]
struct Buildinfo
{
    [DataMember(Order = 0)]
    public string GameVersion;
    [DataMember(Order = 1)]
    public int InternalGameVersion;
    [DataMember(Order = 2)]
    public string GlobalInfoUrl;
    [DataMember(Order = 3)]
    public string GlobalInfoId;
    [DataMember(Order = 4)]
    public int SDKPlatform;
    [DataMember(Order = 5)]
    public string GameLanguage;
    [DataMember(Order = 6)]
    public bool AddGameLanguageDir;
    [DataMember(Order = 7)]
    public string IsUpdateAppFromOfficialUrl;
    [DataMember(Order = 8)]
    public bool IsInputAccountLogin;
    [DataMember(Order = 9)]
    public int GameKind;
}