# PCGameTools

#### 介绍
Windows客户端游戏 快速多启动工具

#### 软件架构
WinForm技术开发


#### 安装教程



#### 使用说明

1. 解压svn_command
2. 修改config.ini，填写正确的pc 客户端的svn路径、要创建的客户端的个数以及名称前缀。
3. 打开PCGameTools.exe，点击创建客户端，选择解压出来的svn_command文件夹。
4、创建客户端，等待完全创建好，启动客户端。
5、可以选择特定的渠道标识，也提供了打开浏览器测试H5游戏的功能。
