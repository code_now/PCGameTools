﻿using System.Windows.Forms;

partial class MainForm
{
    /// <summary>
    /// 必需的设计器变量。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 清理所有正在使用的资源。
    /// </summary>
    /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows 窗体设计器生成的代码

    /// <summary>
    /// 设计器支持所需的方法 - 不要
    /// 使用代码编辑器修改此方法的内容。
    /// </summary>
    private void InitializeComponent()
    {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("1");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("2");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("3");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("4");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("5");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("6");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("7");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("8");
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("9");
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("10");
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("11");
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("12");
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("13");
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("14");
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("15");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btn_create_client = new System.Windows.Forms.Button();
            this.btn_clearaccounts = new System.Windows.Forms.Button();
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_closeclient = new System.Windows.Forms.Button();
            this.input_resolutionx = new System.Windows.Forms.TextBox();
            this.input_resolutiony = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_svnupdate = new System.Windows.Forms.Button();
            this.combox_globalinfo = new System.Windows.Forms.ComboBox();
            this.btn_toggle = new System.Windows.Forms.Button();
            this.checkbox_reopen = new System.Windows.Forms.CheckBox();
            this.btn_add_client = new System.Windows.Forms.Button();
            this.radiobtn_pingpu = new System.Windows.Forms.RadioButton();
            this.radiobtn_cengdie = new System.Windows.Forms.RadioButton();
            this.listview_c = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.combo_opentype = new System.Windows.Forms.ComboBox();
            this.combox_kind = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_create_client
            // 
            this.btn_create_client.Location = new System.Drawing.Point(10, 5);
            this.btn_create_client.Name = "btn_create_client";
            this.btn_create_client.Size = new System.Drawing.Size(100, 25);
            this.btn_create_client.TabIndex = 0;
            this.btn_create_client.Text = "创建客户端";
            this.btn_create_client.UseVisualStyleBackColor = true;
            this.btn_create_client.Click += new System.EventHandler(this.btn_create_client_Click);
            // 
            // btn_clearaccounts
            // 
            this.btn_clearaccounts.Location = new System.Drawing.Point(10, 80);
            this.btn_clearaccounts.Name = "btn_clearaccounts";
            this.btn_clearaccounts.Size = new System.Drawing.Size(100, 25);
            this.btn_clearaccounts.TabIndex = 1;
            this.btn_clearaccounts.Text = "清除账号";
            this.btn_clearaccounts.UseVisualStyleBackColor = true;
            this.btn_clearaccounts.Click += new System.EventHandler(this.btn_clearaccounts_Click);
            // 
            // btn_start
            // 
            this.btn_start.Font = new System.Drawing.Font("宋体", 9F);
            this.btn_start.Location = new System.Drawing.Point(10, 221);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(100, 54);
            this.btn_start.TabIndex = 3;
            this.btn_start.Text = "启动游戏";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_closeclient
            // 
            this.btn_closeclient.Location = new System.Drawing.Point(10, 280);
            this.btn_closeclient.Name = "btn_closeclient";
            this.btn_closeclient.Size = new System.Drawing.Size(100, 25);
            this.btn_closeclient.TabIndex = 4;
            this.btn_closeclient.Text = "关闭客户端";
            this.btn_closeclient.UseVisualStyleBackColor = true;
            this.btn_closeclient.Click += new System.EventHandler(this.btn_closeclient_Click);
            // 
            // input_resolutionx
            // 
            this.input_resolutionx.Location = new System.Drawing.Point(10, 110);
            this.input_resolutionx.MaxLength = 4;
            this.input_resolutionx.Name = "input_resolutionx";
            this.input_resolutionx.Size = new System.Drawing.Size(40, 21);
            this.input_resolutionx.TabIndex = 6;
            this.input_resolutionx.Text = "800";
            this.input_resolutionx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // input_resolutiony
            // 
            this.input_resolutiony.Location = new System.Drawing.Point(70, 110);
            this.input_resolutiony.MaxLength = 4;
            this.input_resolutiony.Name = "input_resolutiony";
            this.input_resolutiony.Size = new System.Drawing.Size(40, 21);
            this.input_resolutiony.TabIndex = 7;
            this.input_resolutiony.Text = "450";
            this.input_resolutiony.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "x";
            // 
            // btn_svnupdate
            // 
            this.btn_svnupdate.Location = new System.Drawing.Point(10, 55);
            this.btn_svnupdate.Name = "btn_svnupdate";
            this.btn_svnupdate.Size = new System.Drawing.Size(100, 25);
            this.btn_svnupdate.TabIndex = 15;
            this.btn_svnupdate.Text = "SVN更新客户端";
            this.btn_svnupdate.UseVisualStyleBackColor = true;
            this.btn_svnupdate.Click += new System.EventHandler(this.btn_svnupdate_Click);
            // 
            // combox_globalinfo
            // 
            this.combox_globalinfo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combox_globalinfo.Location = new System.Drawing.Point(10, 174);
            this.combox_globalinfo.Name = "combox_globalinfo";
            this.combox_globalinfo.Size = new System.Drawing.Size(80, 20);
            this.combox_globalinfo.TabIndex = 16;
            this.combox_globalinfo.SelectedIndexChanged += new System.EventHandler(this.combox_globalinfo_SelectedIndexChanged);
            // 
            // btn_toggle
            // 
            this.btn_toggle.Location = new System.Drawing.Point(90, 153);
            this.btn_toggle.Name = "btn_toggle";
            this.btn_toggle.Size = new System.Drawing.Size(20, 42);
            this.btn_toggle.TabIndex = 17;
            this.btn_toggle.Text = "切换";
            this.btn_toggle.UseVisualStyleBackColor = true;
            this.btn_toggle.Click += new System.EventHandler(this.btn_toggle_Click);
            // 
            // checkbox_reopen
            // 
            this.checkbox_reopen.AutoSize = true;
            this.checkbox_reopen.Checked = true;
            this.checkbox_reopen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkbox_reopen.Font = new System.Drawing.Font("宋体", 9F);
            this.checkbox_reopen.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.checkbox_reopen.Location = new System.Drawing.Point(47, 257);
            this.checkbox_reopen.Name = "checkbox_reopen";
            this.checkbox_reopen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkbox_reopen.Size = new System.Drawing.Size(60, 16);
            this.checkbox_reopen.TabIndex = 20;
            this.checkbox_reopen.Text = "先关闭";
            this.checkbox_reopen.UseVisualStyleBackColor = true;
            this.checkbox_reopen.CheckedChanged += new System.EventHandler(this.checkbox_reopen_CheckedChanged);
            // 
            // btn_add_client
            // 
            this.btn_add_client.Location = new System.Drawing.Point(10, 30);
            this.btn_add_client.Name = "btn_add_client";
            this.btn_add_client.Size = new System.Drawing.Size(100, 25);
            this.btn_add_client.TabIndex = 21;
            this.btn_add_client.Text = "追加客户端";
            this.btn_add_client.UseVisualStyleBackColor = true;
            this.btn_add_client.Click += new System.EventHandler(this.btn_add_client_Click);
            // 
            // radiobtn_pingpu
            // 
            this.radiobtn_pingpu.AutoSize = true;
            this.radiobtn_pingpu.Location = new System.Drawing.Point(10, 135);
            this.radiobtn_pingpu.Name = "radiobtn_pingpu";
            this.radiobtn_pingpu.Size = new System.Drawing.Size(47, 16);
            this.radiobtn_pingpu.TabIndex = 22;
            this.radiobtn_pingpu.Text = "平铺";
            this.radiobtn_pingpu.UseVisualStyleBackColor = true;
            this.radiobtn_pingpu.CheckedChanged += new System.EventHandler(this.radiobtn_pingpu_CheckedChanged);
            // 
            // radiobtn_cengdie
            // 
            this.radiobtn_cengdie.AutoSize = true;
            this.radiobtn_cengdie.Location = new System.Drawing.Point(63, 135);
            this.radiobtn_cengdie.Name = "radiobtn_cengdie";
            this.radiobtn_cengdie.Size = new System.Drawing.Size(47, 16);
            this.radiobtn_cengdie.TabIndex = 23;
            this.radiobtn_cengdie.Text = "层叠";
            this.radiobtn_cengdie.UseVisualStyleBackColor = true;
            this.radiobtn_cengdie.CheckedChanged += new System.EventHandler(this.radiobtn_cengdie_CheckedChanged);
            // 
            // listview_c
            // 
            this.listview_c.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listview_c.CheckBoxes = true;
            this.listview_c.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            listViewItem1.StateImageIndex = 0;
            listViewItem2.StateImageIndex = 0;
            listViewItem3.StateImageIndex = 0;
            listViewItem4.StateImageIndex = 0;
            listViewItem5.StateImageIndex = 0;
            listViewItem6.StateImageIndex = 0;
            listViewItem7.StateImageIndex = 0;
            listViewItem8.StateImageIndex = 0;
            listViewItem9.StateImageIndex = 0;
            listViewItem10.StateImageIndex = 0;
            listViewItem11.StateImageIndex = 0;
            listViewItem12.StateImageIndex = 0;
            listViewItem13.StateImageIndex = 0;
            listViewItem14.StateImageIndex = 0;
            listViewItem15.StateImageIndex = 0;
            this.listview_c.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15});
            this.listview_c.Location = new System.Drawing.Point(10, 311);
            this.listview_c.Name = "listview_c";
            this.listview_c.Size = new System.Drawing.Size(100, 80);
            this.listview_c.TabIndex = 24;
            this.listview_c.UseCompatibleStateImageBehavior = false;
            this.listview_c.View = System.Windows.Forms.View.List;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 40;
            // 
            // combo_opentype
            // 
            this.combo_opentype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_opentype.Location = new System.Drawing.Point(10, 198);
            this.combo_opentype.Name = "combo_opentype";
            this.combo_opentype.Size = new System.Drawing.Size(100, 20);
            this.combo_opentype.TabIndex = 25;
            this.combo_opentype.SelectedIndexChanged += new System.EventHandler(this.combo_opentype_SelectedIndexChanged);
            // 
            // combox_kind
            // 
            this.combox_kind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combox_kind.Location = new System.Drawing.Point(10, 154);
            this.combox_kind.Name = "combox_kind";
            this.combox_kind.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.combox_kind.Size = new System.Drawing.Size(80, 20);
            this.combox_kind.TabIndex = 10;
            this.combox_kind.SelectedIndexChanged += new System.EventHandler(this.combox_kind_SelectedIndexChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(120, 396);
            this.Controls.Add(this.combo_opentype);
            this.Controls.Add(this.listview_c);
            this.Controls.Add(this.radiobtn_cengdie);
            this.Controls.Add(this.radiobtn_pingpu);
            this.Controls.Add(this.btn_add_client);
            this.Controls.Add(this.checkbox_reopen);
            this.Controls.Add(this.btn_toggle);
            this.Controls.Add(this.combox_globalinfo);
            this.Controls.Add(this.btn_svnupdate);
            this.Controls.Add(this.combox_kind);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.input_resolutiony);
            this.Controls.Add(this.input_resolutionx);
            this.Controls.Add(this.btn_closeclient);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.btn_clearaccounts);
            this.Controls.Add(this.btn_create_client);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Opacity = 0.9D;
            this.Text = "PCGameTools";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private Button btn_create_client;
    private Button btn_clearaccounts;
    private Button btn_start;
    private Button btn_closeclient;
    private TextBox input_resolutionx;
    private TextBox input_resolutiony;
    private Label label1;
    private Button btn_svnupdate;
    private ComboBox combox_globalinfo;
    private Button btn_toggle;
    private CheckBox checkbox_reopen;
    private Button btn_add_client;
    private RadioButton radiobtn_pingpu;
    private RadioButton radiobtn_cengdie;
    private ListView listview_c;
    private ColumnHeader columnHeader1;
    private ComboBox combo_opentype;
    private ComboBox combox_kind;
}