﻿using System;
using System.Windows.Forms;

class Log
{
    private static TextBox _log_txt = null;
    public static void set_log_txt(TextBox txt)
    {
        _log_txt = txt;
    }
    public static void log(string s)
    {
        if (_log_txt == null)
        {
            //Console.WriteLine("打印日志需要设置log_txt");
            return;
        }
        _log_txt.AppendText(s);
    }
}
