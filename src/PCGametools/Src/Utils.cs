﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

class Utils
{
    /// <summary>
    /// 执行cmd命令
    /// 多命令请使用批处理命令连接符：
    /// <![CDATA[
    /// &:同时执行两个命令
    /// |:将上一个命令的输出,作为下一个命令的输入
    /// &&：当&&前的命令成功时,才执行&&后的命令
    /// ||：当||前的命令失败时,才执行||后的命令]]>
    /// 其他请百度
    /// </summary>
    /// <param name="cmd"></param>
    public static void do_cmd(string cmd)
    {
        try
        {
            string CmdPath = @"C:\Windows\System32\cmd.exe";
            cmd = cmd.Trim().TrimEnd('&') + "&exit";//说明：不管命令是否成功均执行exit命令，否则当调用ReadToEnd()方法时，会处于假死状态
            using (Process p = new Process())
            {
                p.StartInfo.FileName = CmdPath;
                p.StartInfo.UseShellExecute = false;        //是否使用操作系统shell启动
                p.StartInfo.RedirectStandardInput = true;   //接受来自调用程序的输入信息
                p.StartInfo.RedirectStandardOutput = true;  //由调用程序获取输出信息
                p.StartInfo.RedirectStandardError = true;   //重定向标准错误输出
                p.StartInfo.CreateNoWindow = true;          //不显示程序窗口
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.ErrorDialog = true;
                p.Start();//启动程序

                //向cmd窗口写入命令
                p.StandardInput.WriteLine(cmd);
                p.StandardInput.AutoFlush = true;

                //获取cmd窗口的输出信息
                string output = p.StandardOutput.ReadToEnd();
                System.Console.WriteLine(output);
                Log.log(output);
                p.WaitForExit();//等待程序执行完退出进程
                p.Close();
            }
        }
        catch (Exception ex)
        {
            System.Windows.Forms.MessageBox.Show("执行失败 错误原因:" + ex.Message);
        }
    }

    public static void do_bat(string bat_path)
    {
        try
        {
            string str = System.Windows.Forms.Application.StartupPath + "\\1.bat";

            string strDirPath = System.IO.Path.GetDirectoryName(str);
            string strFilePath = System.IO.Path.GetFileName(str);

            string targetDir = string.Format(strDirPath);//this is where mybatch.bat lies
            Process proc = new Process();
            proc.StartInfo.WorkingDirectory = targetDir;
            proc.StartInfo.FileName = strFilePath;

            //proc.StartInfo.CreateNoWindow = true;
            //proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
            proc.WaitForExit();


            System.Windows.Forms.MessageBox.Show("执行成功");
        }
        catch (Exception ex)
        {
            System.Windows.Forms.MessageBox.Show("执行失败 错误原因:" + ex.Message);
        }
    }

    //打开exe，设置坐标和大小
    [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "MoveWindow")]
    public static extern bool MoveWindow(System.IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
    public static void do_file_open_xy_size(string file_path, int X, int Y, int nWidth, int nHeight)
    {
        Process p = new Process();
        p.StartInfo.FileName = file_path;
        p.StartInfo.CreateNoWindow = true;
        p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
        new Thread(() =>
        {
            p.Start();
            p.WaitForInputIdle();
            Thread.Sleep(1000);
            if (p != null && p.MainWindowHandle != System.IntPtr.Zero)
            {
                MoveWindow(p.MainWindowHandle, X, Y, nWidth, nHeight, true);
            }
        }).Start();
    }
}
